﻿//
//  AttachmentsService.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using System.Collections.Generic;
using Hackathon.ServiceModel;
using System.Linq;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel.Types;

namespace Hackathon.ServiceInterface
{
	public class AttachmentsService : Service
	{
		[Authenticate]
		public List<AttachmentResponse> Get (GetChallengeAttachments request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");
			
			//Retrieve the challenge from the database
			var challenge = ChallengesService.GetChallengeById (this, request.Id, true);

			if (challenge.Attachments.IsNullOrEmpty ())
				return new List<AttachmentResponse> ();

			var ret = Db.Select<Attachment> (a => challenge.Attachments.Contains (a.Id)).Select (a => a.ConvertTo<AttachmentResponse> ()).ToList ();

			//log the access to the attachments
			ChallengesService.LogChallengeAccess (Db, request.Id, loggedInUserId, AccessType.Attachments);

			return ret;
		}

		[Authenticate]
		public List<AttachmentResponse> Get (GetHintAttachments request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			//Retrieve the challenge from the database
			var challenge = ChallengesService.GetChallengeById (this, request.Id, true);

			//Retrieve the hint from the database
			var hints = Db.Select<Hint> (h => h.Id == request.HintNumber && h.ChallengeId == challenge.Id);
			if (!hints.Any ())
				throw HttpError.NotFound ("No such hint");

			Hint hint = hints.First ();

			if (hint.Attachments.IsNullOrEmpty ())
				return new List<AttachmentResponse> ();

			var ret = Db.Select<Attachment> (a => hint.Attachments.Contains (a.Id)).Select (a => a.ConvertTo<AttachmentResponse> ()).ToList ();

			//log the access to the attachments
			ChallengesService.LogHintAccess (Db, request.Id, loggedInUserId, request.HintNumber);

			return ret;
		}
	}
}

