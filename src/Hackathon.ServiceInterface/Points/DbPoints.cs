﻿//
//  DbPoints.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Linq;
using ServiceStack.Data;
using ServiceStack;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel;
using Hackathon.ServiceModel.Types;
using System.Collections.Generic;

namespace Hackathon.ServiceInterface.Points
{
	public class DbPoints : IPoints
	{
		public int GetPoints (Service service, int userId, int challengeId)
		{
			using (var userService = service.ResolveService<UsersService> ()) {
				var userChallenge = userService.Get (new GetUserChallenge () {
					UserId = userId,
					ChallengeId = challengeId
				});

				//First check, if the challenge has even been solved yet
				DateTime solvedDate = DateTime.MinValue;
				if (!userChallenge.SolvedDate.IsNullOrEmpty ())
					solvedDate = userChallenge.SolvedDate.FromEcmaScriptString ();

				DateTime accessDate = DateTime.MinValue;
				if (!userChallenge.AccessDate.IsNullOrEmpty ())
					accessDate = userChallenge.AccessDate.FromEcmaScriptString ();

				// Get all hint IDs from the database which have ben accessed
				var accessedHintIds = service.Db.Select<HintAccess> (h => (
				                          (h.ChallengeId == challengeId) &&
				                          (h.UserId == userId)
				                      ))
					.Where (h => h.AccessTime.ToUniversalTime () < solvedDate.ToUniversalTime ())  // We only want hints, which have been accessed before the challenge was solved. We do this here instead of the db query, because different timezones could lead to wrong results otherwise
					.Select (h => h.HintId);  // We only want the IDs of the hints

				return GetPoints (service, userId, challengeId, solvedDate, accessDate, accessedHintIds);
			}
		}

		public int GetPoints (Service service, int userId, int challengeId, DateTime solvedDate, DateTime accessDate, IEnumerable<int> accessedHintIds)
		{
			/* Point calculation system:
            The Challenge table holds a BasePoints value
            A speed bonus is awarded, if the challenge is solved during the first half of its runtime (full days, rounded up)
            The maximal speed bonus is BasePoints*0.4. After n hours you get (BasePoints*0.4 * (max_days*24-floor(n)))/(max_days*24) points
            The Hint table gets a new column Value, which holds the percentage of BasePoints ('50' for 50%) the hint will subtract from the points
            Using any hints will completely remove the speed bonus
            The points can not be negative
            */
			using (var userService = service.ResolveService<UsersService> ()) {
				if (solvedDate == DateTime.MinValue)
					//Not solved yet -> return 0 points
					return 0;
				
				//Fetch full informations about the challenge from the database
				Challenge challenge = ChallengesService.GetChallengeById (service, challengeId, false);

				int basePoints = challenge.BasePoints;
				int points = basePoints;

				//Retrieve information about the challenge from the database
				using (var challengeSetsService = service.ResolveService<ChallengeSetsService> ()) {
					//Find out which challengeset the challenge belongs to
					var challengeSetsWithEvent = challengeSetsService.Get (new FindChallengeSets (){ ChallengeId = challengeId });
					if (!challengeSetsWithEvent.Any ())
						throw HttpError.NotFound ("Challenge is part of any ChallengeSet");
					//Find out which event the challengeset belongs to were the user is a member of that event

					// Get all events ths user is part of
					IEnumerable<int> userEventIds = service.Db.ColumnDistinct<int> (service.Db.From<EventUsers> ().Where (e => (e.UserId == userId)).Select (e => e.EventId));

					int eventId;
					int challengeSetId;
					bool eventFound = false;
					using (var eventsService = service.ResolveService<EventsService> ()) {
						foreach (int challengeSetCandidate in challengeSetsWithEvent) {
							var eventsWithChallengeSet = eventsService.Get (new FindEvents (){ ChallengeSetId = challengeSetCandidate });
							var eventsWithChallengeSetAndUser = eventsWithChallengeSet.Intersect (userEventIds);
							int eventCandidates = eventsWithChallengeSetAndUser.Count ();
							if (eventCandidates == 1) {
								if (eventFound)
									throw HttpError.Conflict ("This challenge is part of multiple events of that user. Cannot calculate the points.");
								else {
									eventId = eventsWithChallengeSetAndUser.First ();
									challengeSetId = challengeSetCandidate;
									eventFound = true;
								}
							} else if (eventCandidates > 1)
								throw HttpError.Conflict ("This challenge is part of multiple events of that user. Cannot calculate the points.");
						}
						if (!eventFound)
							throw HttpError.NotFound ("Challenge is not part of any event of that user.");

						EventChallengeSets challengeSetInEvent = service.Db.Single<EventChallengeSets> (e => (e.EventId == eventId) && (e.ChallengeSetId == challengeSetId));

						//Check if the solution was on time
						if (solvedDate.ToUniversalTime () > challengeSetInEvent.EndDate.ToUniversalTime ())
							//Solution came after the challenge expired
							return 0;

						//calulate the speed bonus (if any)
						TimeSpan maxTime = challengeSetInEvent.EndDate.Subtract (challengeSetInEvent.StartDate);
						int speedBonusDays = (int)Math.Round ((double)(maxTime.Days / 2));
						int speedBonusHours = speedBonusDays * 24;

						TimeSpan solvingTime = new TimeSpan (0);
						if (accessDate != DateTime.MinValue)
							solvingTime = solvedDate.ToUniversalTime ().Subtract (accessDate.ToUniversalTime ());
						int speedBonus = 0;
						if (solvingTime.TotalHours < speedBonusHours) {
							//User is eglible for a speed bonus
							int maxSpeedBonus = (int)(challenge.BasePoints * 0.4);
							speedBonus = (int)(maxSpeedBonus * (speedBonusHours - Math.Floor (solvingTime.TotalHours)) / speedBonusHours);
						}
						points += speedBonus;

						//Check which hints have been accessed by the user
						if (accessedHintIds != null && accessedHintIds.Any ()) {
							//if hints have been accessed, the speed bonus will be removed
							points -= speedBonus;
							//deduct the defined percentage value for each hint
							foreach (var hintId in accessedHintIds) {
								Hint hint = service.Db.Single<Hint> (h => h.Id == hintId);
								points -= basePoints * hint.PointReduction / 100;
							}
						}
					}
				}
				//Make sure, that the points are not negative
				if (points < 0)
					points = 0;

				return points;
			}
		}
	}
}

