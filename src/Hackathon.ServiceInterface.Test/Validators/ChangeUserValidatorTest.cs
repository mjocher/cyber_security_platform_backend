﻿//
//  ChangeUserValidatorTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NUnit.Framework;
using System;
using Hackathon.ServiceModel.Validators;
using Hackathon.ServiceModel;

namespace Hackathon.ServiceInterface.Test.Validators
{
	[TestFixture ()]
	public class ChangeUserValidatorTest
	{
		[Test ()]
		public void ValidateAvatarTest ()
		{
			ChangeUserValidator validator = new ChangeUserValidator ();
			Assert.That (validator.Validate (new ChangeUser {
				Avatar = @"data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9TXL0Y4OHwAAAABJRU5ErkJggg==",
			}).IsValid);

		}

		[Test ()]
		public void ValidateAvatarEmptyTest ()
		{
			ChangeUserValidator validator = new ChangeUserValidator ();
			Assert.That (validator.Validate (new ChangeUser {
				Avatar = "",
			}).IsValid);

		}

		[Test ()]
		public void ValidateAvatarNullTest ()
		{
			ChangeUserValidator validator = new ChangeUserValidator ();
			Assert.That (validator.Validate (new ChangeUser {
				Avatar = null,
			}).IsValid);

		}

		[Test ()]
		public void ValidateAvatarBadCharacterTest ()
		{
			ChangeUserValidator validator = new ChangeUserValidator ();
			Assert.That (validator.Validate (new ChangeUser {
				Avatar = @"data:image/png;base64, $$$iVBORw0KGgoAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9TXL0Y4OHwAAAABJRU5ErkJggg==",
			}).IsValid, Is.False);

		}

		[Test ()]
		public void ValidateAvatarOnlyBase64Test ()
		{
			ChangeUserValidator validator = new ChangeUserValidator ();

			Assert.That (validator.Validate (new ChangeUser {
				Avatar = @"iVBORw0KGgoAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9TXL0Y4OHwAAAABJRU5ErkJggg==",
			}).IsValid, Is.False);

		}
	}
}

