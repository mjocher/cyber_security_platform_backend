﻿//
//  Attachments.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using ServiceStack;

namespace Hackathon.ServiceModel
{
	[Route ("/challenges/{id}/attachments", "GET")]
	public class GetChallengeAttachments : IReturn<List<AttachmentResponse>>
	{
		public int Id { get; set; }
	}

	[Route ("/challenges/{id}/hints/{hintnumber}/attachments", "GET")]
	public class GetHintAttachments : IReturn<List<AttachmentResponse>>
	{
		public int Id { get; set; }

		public int HintNumber { get; set; }
	}

	public class AttachmentResponse
	{
		public string Title { get; set; }

		public string MimeType { get; set; }

		public List<byte> Content { get; set; }
	}
}

